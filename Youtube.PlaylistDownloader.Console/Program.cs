﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Youtube.PlaylistDownloader.Console
{
	public class Program
	{
		public static void Main(string[] args)
		{
			Trace.Listeners.Add(new ConsoleTraceListener());

			PrintOptions();

			string choiceString = System.Console.ReadLine();
			int choice;

			CheckForShutdown(choiceString);

			while (string.IsNullOrWhiteSpace(choiceString) || 
			       !int.TryParse(choiceString, out choice) ||
			       choice < 1 ||
			       choice > 2)
			{
				System.Console.WriteLine("Your selection is invalid.");
				System.Console.WriteLine();
				PrintOptions();

				choiceString = System.Console.ReadLine();

				CheckForShutdown(choiceString);
			}

			switch (choice)
			{
				case 1:
					DownloadPlaylist();
					break;

				case 2:
					DownloadVideo();
					break;
			}
		}

		private static void PrintOptions()
		{
			System.Console.WriteLine("Welcome to EwneoN's YouTube Playlist downloader!");
			System.Console.WriteLine("Please select an option from below:");
			System.Console.WriteLine("1.\tDownload Playlist");
			System.Console.WriteLine("2.\tDownload Single Video");
			System.Console.WriteLine("Q.\tQuit");
		}

		private static void CheckForShutdown(string choiceString)
		{
			if (choiceString?.StartsWith("Q", StringComparison.OrdinalIgnoreCase) ?? false)
			{
				Shutdown();
			}
		}

		private static void Shutdown()
		{
			System.Console.WriteLine("Goodbye");
			Thread.Sleep(TimeSpan.FromSeconds(2));
			Environment.Exit(0);
		}

		private static void DownloadPlaylist()
		{
			System.Console.WriteLine("Please enter the url of the youtube playlist you wish to download");

			string url = System.Console.ReadLine();

			if (!Uri.TryCreate(url, UriKind.Absolute, out Uri uri))
			{
				throw new ArgumentException("Not a valid uri", nameof(url));
			}

			System.Console.WriteLine("Please enter the path to the directory to store the videos");

			string directory = System.Console.ReadLine();

			Downloader downloader = new Downloader();
			Task.WaitAll(downloader.DownloadPlaylistToDirectory(uri, directory));
		}

		private static void DownloadVideo()
		{
			System.Console.WriteLine("Please enter the url of the youtube video you wish to download");

			string url = System.Console.ReadLine();

			System.Console.WriteLine("Please enter the path to the directory to store the video");

			string directory = System.Console.ReadLine();

			Downloader downloader = new Downloader();
			downloader.DownloadVideoToDirectory(url, directory);
		}
	}
}
