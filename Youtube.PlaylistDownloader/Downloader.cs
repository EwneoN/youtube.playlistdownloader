﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using HtmlAgilityPack;
using YoutubeExtractor;

namespace Youtube.PlaylistDownloader
{
	public class Downloader
	{
		private const string PlaylistVideoLinkXpath = "//a[contains(@class,'pl-video-title-link')]";

		public async Task DownloadPlaylistToDirectory(Uri playlistUrl, string directory)
		{
			string response;

			using (HttpClient client = new HttpClient())
			{
				response = await client.GetStringAsync(playlistUrl);

				if (string.IsNullOrWhiteSpace(response))
				{
					Trace.TraceError("Response was empty");
					return;
				}
			}

			HtmlDocument document = new HtmlDocument();
			document.LoadHtml(response);

			HtmlNodeCollection videoNodes = document.DocumentNode.SelectNodes(PlaylistVideoLinkXpath);

			for (int i = 0; i < videoNodes.Count; i++)
			{
				try
				{
					string href = videoNodes[i].Attributes["href"]?.Value;

					if (string.IsNullOrWhiteSpace(href))
					{
						Trace.TraceError($"No href found on node[{i}]");
						continue;
					}

					DownloadVideoToDirectory(href, directory);
				}
				catch (Exception ex)
				{
					Trace.TraceError(ex.ToString());
				}
			}
		}

		public void DownloadVideoToDirectory(string videoUrl, string directory)
		{
			IEnumerable<VideoInfo> videoInfos = DownloadUrlResolver.GetDownloadUrls(videoUrl);

			VideoInfo video = videoInfos
				.Where(info => info.VideoType == VideoType.Mp4)
			  .OrderByDescending(info => info.Resolution)
			  .FirstOrDefault();

			if (video == null)
			{
				Trace.TraceError($"Failed to get an Mp4 version for {videoUrl}");
				return;
			}

			if (video.RequiresDecryption)
			{
				DownloadUrlResolver.DecryptDownloadUrl(video);
			}

			string title = video.Title;
			var invalidChars = Path.GetInvalidFileNameChars();

			foreach (char invalidChar in invalidChars)
			{
				title = title.Replace(invalidChar.ToString(), "");
			}

			VideoDownloader downloader = new VideoDownloader(video, Path.Combine(directory, title + video.VideoExtension));

			downloader.DownloadProgressChanged += (sender, args) => Trace.TraceInformation($"{title}: {args.ProgressPercentage:#0.00}%");

			downloader.Execute();
		}
	}
}