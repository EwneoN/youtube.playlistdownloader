A simple console application for downloading YouTube playlists and videos

In order to download the latest release go to:
https://gitlab.com/EwneoN/youtube.playlistdownloader/pipelines

The most recent version can be downloaded by clicking the download artifact 
button for the most recent successful pipeline.
![](download-instruction-screenshot.png)

After downloading and extracting the latest release, browse to the 
Releases\Youtube.PlaylistDownloader.Console folder and double click 
Youtube.PlaylistDownloader.Console.exe to launch the application.

# YoutubeExtractor

Cheers to flagbug on GitHub for the actual YoutubeExtractor project.
This project uses the source code from:
https://github.com/flagbug/YoutubeExtractor

Downloaded and added to this project on 2019-02-16. I don't use the provided 
nuget package as it is currently breaking on certain URLs and the code is not 
maintained regularly enough for my needs.

If you love this project, please consider a donation to flagbug for providing
the core code.

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=daume%2edennis%40gmail%2ecom&lc=US&item_name=YoutubeExtractor&no_note=0&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donate_LG%2egif%3aNonHostedGuest">
  <img src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif" title="Donate via Paypal" />
</a>

<a href="http://flattr.com/thing/1093085/" target="_blank">
<img src="http://api.flattr.com/button/flattr-badge-large.png" alt="Flattr this" title="Flattr this" border="0" />
</a>